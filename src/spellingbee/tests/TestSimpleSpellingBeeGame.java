package spellingbee.tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import spellingbee.server.SimpleSpellingBeeGame;

/**
 * This JUnit Test Case class tests the SimpleSpellingBeeGame class used in developping the front end.
 * @author Vladyslav Berezhnyak
 *
 */
class TestSimpleSpellingBeeGame 
{
	//global variable game to be used everywhere
	private SimpleSpellingBeeGame game = new SimpleSpellingBeeGame();
	
	/**
	 * This method tests if a word submitted returns the appropriate amount of points.
	 */
	@Test
	void testGetPointsForWords() 
	{
		int word1 = game.getPointsForWords("beam");
		assertEquals(word1, 1);
		
		int word2 = game.getPointsForWords("bemad");
		assertEquals(word2, 5);
		
		int word3 = game.getPointsForWords("gambled");
		assertEquals(word3, 14);
		
		int word4 = game.getPointsForWords("foo");
		assertEquals(word4, 0);
	}
	
	/**
	 * This method tests if a word is valid according to weather it contains the center letter.
	 */
	@Test
	void testGetMessage() 
	{
		String word1 = game.getMessage("beam");
		assertEquals(word1, "valid");
		
		String word2 = game.getMessage("beal");
		assertEquals(word2, "invalid");
		
		String word3 = game.getMessage("eadlbg");
		assertEquals(word3, "invalid");
		
		String word4 = game.getMessage("gambled");
		assertEquals(word4, "valid");
		
		String word5 = game.getMessage("gamblet");
		assertEquals(word5, "invalid");
		
		String word6 = game.getMessage("cat");
		assertEquals(word6, "invalid");
	}
}
