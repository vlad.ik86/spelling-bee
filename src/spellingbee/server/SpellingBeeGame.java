package spellingbee.server;
import java.io.*;
import java.util.*;

import spellingbee.server.ISpellingBeeGame;

/**
 * This is the Spelling Bee game itself, where most of the magic and calculation happens
 * @author Luke Weaver
 */

public class SpellingBeeGame implements ISpellingBeeGame{
	//the String with the usable letters
	private String letters;
	//the center letter
	private char centerLetter;
	//the user's score
	private int score = 0;
	//the words the user has already entered
	private ArrayList<String> history = new ArrayList<String>();
	//the list of all combinations of letters the game may use
	private ArrayList<String> combos = createWordsFromFile("datafiles\\letterCombinations.txt");
	//the list of possible valid words the provided letters
	private ArrayList<String> possibleWords;
	private Random randGen = new Random();
	
	/**
	 * A constructor that sets the private fields up
	 */
	public SpellingBeeGame() {
		this.letters = this.combos.get(randGen.nextInt(this.combos.size()));
		this.centerLetter = this.letters.charAt(randGen.nextInt(letters.length()));
		this.possibleWords = getPossibleWords(createWordsFromFile("datafiles\\english.txt"));
	}
	
	/**
	 * A constructor that creates a game with a specified letter set
	 * @param letters the letters that can be used
	 */
	public SpellingBeeGame(String letters) {
		this.letters = letters;
	}
	
	/**
	 * adds a word to the history array
	 * @param word the word to be added
	 */
	public void addtoHistory(String word) {
		this.history.add(word);
	}
	
	/**
	 * returns the amount of points received for a given word as per the rules of the game
	 * @param attempt the word checking the points for
	 * @return points the number of points
	 */
	@Override
    public int getPointsForWords(String attempt) {
        int points = 0;
        
        //if the word is 4 letters, 1 point
        if(attempt.length() == 4){
            points++;
            this.score += points;
        //if the word is more than 4 letters, check if it is a panagram
        }else if(attempt.length() > 4){
            if(containsAll(attempt)){
                points += 7;
            }
            //if not, points by word length
            points += attempt.length();
            this.score += points;
        }
        return points;
    }
	
	/**
	 * checks if a word is a panagram, meaning it contains all the letters
	 * @param attempt the word being checked
	 * @return a boolean whether or not the word does
	 */
	public boolean containsAll(String attempt) {
        char[] letters = getAllLetters().toCharArray();
        
        for(int i = 0; i < letters.length; i++){
            if(!attempt.contains("" + letters[i])){
                return false;
            }    
        }
        return true;
	}
	
	/**
	 * returns whether the word is valid or not using possibleWords and history
	 * @return a String stating whether the word is valid or not
	 */
    @Override
    public String getMessage(String attempt) {
        //if the word isn't possible, don't even check anything, not valid
        if(!possibleWords.contains(attempt)) {
        	return "invalid";
        }
        
        //if the word has been entered before it is not valid
        if(this.history.contains(attempt)) {
        	return "invalid";
        }
        
        if(attempt.length() < 4)
        {
        	return "invalid";
        }
        
        //adds it in the case it makes it past the history check
        addtoHistory(attempt);
        
        return "valid";
    }
    
    /**
     * gets all letters usable
     * @return letters the set of usable letters
     */
	@Override
	public String getAllLetters() {
		return this.letters;
	}

	/**
	 * gets the center letter
	 * @return centerLetter the center letter
	 */
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}

	/**
	 * gets the user's score
	 * @return score the user's score
	 */
	@Override
	public int getScore() {
		return this.score;
	}
	
	/**
	 * just like getMessage, except it doesn't check history, allowing for the possibleWords to be found without slowing it down as much
	 * @param attempt
	 * @return a String stating whether the word is valid or not
	 */
	public String checkValid(String attempt) {
        char[] wordChars = attempt.toCharArray();
        char center = getCenterLetter();
        
        //if it doesn't have the center letter
        if(!attempt.contains("" + center)){
            return "invalid";
        }
        
        //if it doesn't have any of the letters
        for(int i = 0; i < wordChars.length; i++){
            if(!this.letters.contains("" + wordChars[i])){
                return "invalid";
            }
        }
        
        return "valid";
    }
	
	/**
	 * given the letters provided, goes through all possible words and removes impossible words and returns a collection of only those words
	 * @param words the words to go through and remove the impossible words
	 * @return possible a collection of the usable words using the letters given
	 */
	public ArrayList<String> getPossibleWords(ArrayList<String> words){
		ArrayList<String> possible = new ArrayList<String>();
		possible = (ArrayList<String>)words.clone();
		
		System.out.println("Finding possible words...");
		
		for(int i = 0; i < possible.size(); i++) {
			if(checkValid(possible.get(i)).equals("invalid")) {
				//marked to be removed
				possible.set(i, "0");
			}
		}
		
		//removes the unwanted words that aren't possible
		while(possible.remove("0")) {
		}
		
		/*for(int i = 0; i < possible.size(); i++) {
			System.out.println(possible.get(i));
		}*/
		
		System.out.println("Done!");
		
		return possible;
	}

	/**
	 * gets the different point brackets depending on the max amount of points possible and returns them as an int[]
	 * @return brackets the brackets possible
	 */
	@Override
	public int[] getBrackets() {
		int max = possibleWords.size();
		//{25%,50%,75%,90%,100%}
		int[] brackets = {(int)(max * 0.25), (int) (max * 0.50), (int) (max * 0.75), (int) (max * 0.9), max};
		
		return brackets;
	}
	
	/**
	 * given a path, goes to the file specified and goes through it, making a word out of every line in that file
	 * @param path the path to the file we are going to make the words from
	 * @return arr a collection of the words from the provided file
	 */
	public ArrayList<String> createWordsFromFile(String path) {
		ArrayList<String> arr = new ArrayList<String>();
		try{
			File file = new File(path);
			Scanner reader = new Scanner(file);
			while (reader.hasNextLine()) {
				String data = reader.nextLine();
			    arr.add(data);
			}
			reader.close();
		}catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		return arr;
	}
}
