package spellingbee.server;
import spellingbee.server.*;

/**
 * This is a simplified version of the main SpellingBeeGame class for UI development purposes. Not meant
 * to be implemented in the final version of the project.
 * @author Vladyslav Berezhnyak
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame
{
	private String letters;
	private char center;
	private int score = 0;
	
	/**
	 * This constructor sets hardcoded values for the 7 letters and the center letter.
	 */
	public SimpleSpellingBeeGame()
	{
		this.letters = "eamdlbg";
		this.center = 'm';
	}

	/**
	 * Simple version of the getPointsForWords() method. Validates how many points a word is worth and adds
	 * it to the global score variable
	 * @author Vladyslav Berezhnyak
	 * @param String of the word
	 * @return Int of how much the word is worth.
	 */
	@Override
	public int getPointsForWords(String attempt) 
	{
		char[] ltrs = getAllLetters().toCharArray();
		int points = 0;
		
		if(attempt.length() == 4)
		{
			points++;
			this.score += points;
			return points;
		}
		
		else if(attempt.length() > 4)
		{
			boolean containsAllLtrs = true;
			for(int i = 0; i < ltrs.length; i++)
			{
				if(!attempt.contains("" + ltrs[i]))
				{
					containsAllLtrs = false;
					break;
				}	
			}
			
			if(containsAllLtrs)
			{
				points += 7;
			}
			
			points += attempt.length();
			this.score += points;
			return points;
		}
		
		else
		{
			return points;
		}
	}

	/**
	 * Simple version of getMessage() method. Simply checks if the word consists of only of the 7 letters.
	 * If not, the word is invalid.
	 * @author Vladyslav Berezhnyak
	 * @param String of the word
	 * @return Message of the validity of the word
	 */
	@Override
	public String getMessage(String attempt) 
	{
		String pattern = getAllLetters();
		char[] wordChars = attempt.toCharArray();
		char cntr = getCenterLetter();
		
		if(!attempt.contains("" + cntr))
		{
			return "invalid";
		}
		
		for(int i = 0; i < wordChars.length; i++)
		{
			if(!pattern.contains("" + wordChars[i]))
			{
				return "invalid";
			}
		}
		
		return "valid";
	}
	
	/**
	 * Simply returns the 7 letters.
	 * @return String of the 7 letters
	 */
	@Override
	public String getAllLetters() 
	{
		return this.letters;
	}

	/**
	 * Simply returns the center letter.
	 * @return Char of the center letter
	 */
	@Override
	public char getCenterLetter() 
	{
		return this.center;
	}

	/**
	 * Simply returns the total user score.
	 * @return Int of the user score
	 */
	@Override
	public int getScore() 
	{
		return this.score;
	}

	/**
	 * This method is used in the GUI application to determine the various point categories.
	 * (Values are hardcoded for testing purposes.)
	 * @author Vladyslav Berezhnyak
	 */
	@Override
	public int[] getBrackets() 
	{
		int[] arr = {0, 2, 6, 9 ,17};
		return arr;
	}
}
