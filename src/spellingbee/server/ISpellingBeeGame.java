package spellingbee.server;

/**
 * This interface is a body for building a Spelling Bee game. It has all the required methods
 * in order to make the game functional.
 * @author Vladyslav Berezhnyak
 */
public interface ISpellingBeeGame 
{
	/**
	 * This method returns the number of points that a word is given according to its length.
	 * @param String of a word
	 * @return Int of the amount of points that the word is worth
	 */
	int getPointsForWords(String attempt);
	
	/**
	 * This method checks if the word given is valid according to the Spelling Bee rules.
	 * Returns a message based on whether the word is valid or invalid.
	 * @param String of a word
	 * @return String of the message saying whether or not the word is valid
	 */
	String getMessage(String attempt);
	
	/**
	 * This method returns the set of 7 letters that the Spelling Bee object is storing.
	 * @return String of the 7 letters
	 */
	String getAllLetters();
	
	/**
	 * This method return the center character that is required to be apart of every word.
	 * @return Char of the center letter
	 */
	char getCenterLetter();
	
	/**
	 * This method returns the current score of the user.
	 * @return Int of the user's score
	 */
	int getScore();
	
	/**
	 * This method is used in the GUI application to determine the various point categories.
	 * @return Int[] of the numbers in order of category
	 */
	int[] getBrackets();
}
