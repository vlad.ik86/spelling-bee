package spellingbee.client.eventlisteners;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.*;


/**
 * This class is an event listener that submits the information found in the Input field to the server.
 * @author Vladyslav Berezhnyak
 *
 */
public class SubmitAction implements EventHandler<ActionEvent>
{
	private TextField input;
	private TextField msgField;
	private TextField pointsField;
	private Client net;
	
	/**
	 * This constructor takes as input the input, the message box, the points box and a Client object.
	 * @param TextField of the input box
	 * @param TextField of the message box
	 * @param TextField of the points box
	 * @param Client object
	 */
	public SubmitAction(TextField vInput, TextField vMsg, TextField vPoints, Client vNet)
	{
		this.input = vInput;
		this.msgField = vMsg;
		this.pointsField = vPoints;
		this.net = vNet;
	}
	
	/**
	 * This handle takes as input a click event from the button.
	 * @param ActionEvent click
	 * @return void
	 */
	@Override
	public void handle(ActionEvent e)
	{
		String query1 = "wordValid;" + this.input.getText();
		String[] servResponse1 = this.net.sendAndWaitMessage(query1).split(";");
		
		String query2 = "getUserScore";
		String[] servResponse2 = this.net.sendAndWaitMessage(query2).split(";");
		
		this.msgField.setText(servResponse1[0]);
		this.pointsField.setText(servResponse2[1]);
	}
}
