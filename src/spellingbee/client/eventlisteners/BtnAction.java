package spellingbee.client.eventlisteners;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.*;

/**
 * This class is an event listener that assigns actions to the letter buttons. When clicked, the button
 * will append the corresponding letter to the input field.
 * @author Vladyslav Berezhnyak
 *
 */
public class BtnAction implements EventHandler<ActionEvent>
{
	private TextField input;
	private String ltr;
	
	/**
	 * This constructor takes as input the input text field and the text from the button clicked.
	 * @param TextField of the input box
	 * @param String of the letter from the button
	 */
	public BtnAction(TextField vInput, String vLtr)
	{
		this.input = vInput;
		this.ltr = vLtr;
	}
	
	/**
	 * This handle method expects a click event from one of the 7 buttons.
	 * @param ActionEvent click
	 * @return void
	 */
	@Override
	public void handle(ActionEvent e)
	{
		this.input.appendText(this.ltr);
	}
}
