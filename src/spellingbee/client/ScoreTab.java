package spellingbee.client;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * this is the score tab, where the brackets point requirements are stored and current score is displayed
 * @author Luke
 *
 */

public class ScoreTab extends Tab{
	private Client cl;
	private Text score = new Text("0");
	private Text bv1 = new Text("0");
	private Text bv2 = new Text("0");
	private Text bv3 = new Text("0");
	private Text bv4 = new Text("0");
	private Text bv5 = new Text("0");
	
	private Text bn1 = new Text("0");
	private Text bn2 = new Text("0");
	private Text bn3 = new Text("0");
	private Text bn4 = new Text("0");
	private Text bn5 = new Text("0");
	
	/**
	 * constructor that creates the score tab
	 * @param cl the client being used
	 */
	public ScoreTab(Client cl) {
		super("Score");
		this.cl = cl;
		this.setContent(new TextField("a"));
		//takes "brackets;x1,x2,x3,x4,x5" from the serverController, and turns it into "x1,x2,x3,x4,x5"
		String svrResp = cl.sendAndWaitMessage("getBracket").split(";")[1];
		int[] brackets = new int[5];
		
		for(int i = 0; i < brackets.length; i++) {
			brackets[i] = Integer.parseInt(svrResp.split(",")[i]);
		}
		
		this.score.setFill(Color.RED);
		
		//bracket names
		this.bn1.setText("OK");
		this.bn1.setFill(Color.GREY);
		this.bn2.setText("Nice");
		this.bn2.setFill(Color.GREY);
		this.bn3.setText("Great");
		this.bn3.setFill(Color.GREY);
		this.bn4.setText("Fantastic");
		this.bn4.setFill(Color.GREY);
		this.bn5.setText("Winner");
		this.bn5.setFill(Color.GREY);
		
		//bracket scores
		this.bv1.setText("" + brackets[0]);
		this.bv2.setText("" + brackets[1]);
		this.bv3.setText("" + brackets[2]);
		this.bv4.setText("" + brackets[3]);
		this.bv5.setText("" + brackets[4]);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		
		//sets up the labels with point brackets and current score
		grid.addRow(5, new Text("Current Score"), this.score);
		grid.addRow(4, this.bn1, this.bv1);
		grid.addRow(3, this.bn2, this.bv2);
		grid.addRow(2, this.bn3, this.bv3);
		grid.addRow(1, this.bn4, this.bv4);
		grid.addRow(0, this.bn5, this.bv5);
		
		this.setContent(grid);
	}
	
	/**
	 * updates the score variable and refreshes its value in-app and also grays out not yet attained bracket scores
	 */
	public void refresh() {
		int score = Integer.parseInt(cl.sendAndWaitMessage("getUserScore").split(";")[1]);
		this.score.setText("" + score);
		
		//level 5, "Winner"
		if(score >= Integer.parseInt(this.bv5.getText())) {
			this.bn5.setFill(Color.BLACK);
		}
		//level 4, "Fantastic"
		if(score >= Integer.parseInt(this.bv4.getText())) {
			this.bn4.setFill(Color.BLACK);
		}
		//level 3, "Great"
		if(score >= Integer.parseInt(this.bv3.getText())) {
			this.bn3.setFill(Color.BLACK);
		}
		//level 2, "Nice"
		if(score >= Integer.parseInt(this.bv2.getText())) {
			this.bn2.setFill(Color.BLACK);
		}
		//level 1, "OK"
		if(score >= Integer.parseInt(this.bv1.getText())) {
			this.bn1.setFill(Color.BLACK);
		}
		
	}
}
