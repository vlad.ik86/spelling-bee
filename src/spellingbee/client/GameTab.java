package spellingbee.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.*;
import spellingbee.network.*;
import spellingbee.client.eventlisteners.*;

/**
 * This class is the front end layout of the UI.
 * @author Vladyslav Berezhnyak
 *
 */
public class GameTab extends Tab
{
	private Client cl;
	private TextField userScore;
	
	/**
	 * This constructor only takes as input the Client object. The rest of it builds the UI and assigns
	 * event listeners for the buttons and input text field.
	 * @param Client object
	 */
	public GameTab(Client net)
	{
		super("Game");
		this.cl = net;
		
		//container and layout
		Group root = new Group();
		
		//server fetch protocols for letters and center
		String[] responseLetters = cl.sendAndWaitMessage("getLetters").split(";");
		String[] responseCenter = cl.sendAndWaitMessage("getCenter").split(";");
		
		//converts letters string into a chars array
		char[] ltrs = responseLetters[1].toCharArray();
		
		//create components
		VBox overall = new VBox();
		
		HBox centerLetter = new HBox();
		TextField msg = new TextField("The center letter is: " + responseCenter[1]);

		HBox buttons = new HBox();
		Button ltr1 = new Button("" + ltrs[0]);
		Button ltr2 = new Button("" + ltrs[1]);
		Button ltr3 = new Button("" + ltrs[2]);
		Button ltr4 = new Button("" + ltrs[3]);
		Button ltr5 = new Button("" + ltrs[4]);
		Button ltr6 = new Button("" + ltrs[5]);
		Button ltr7 = new Button("" + ltrs[6]);
		
		HBox inputWord = new HBox();
		TextField input = new TextField();
		
		HBox submitBtn = new HBox();
		Button submit = new Button("Submit");
		
		HBox statusMsgs = new HBox();
		TextField serverMsg = new TextField();
		this.userScore = new TextField();

		//event listeners
		BtnAction action1 = new BtnAction(input, ltr1.getText());
		ltr1.setOnAction(action1);
		BtnAction action2 = new BtnAction(input, ltr2.getText());
		ltr2.setOnAction(action2);
		BtnAction action3 = new BtnAction(input, ltr3.getText());
		ltr3.setOnAction(action3);
		BtnAction action4 = new BtnAction(input, ltr4.getText());
		ltr4.setOnAction(action4);
		BtnAction action5 = new BtnAction(input, ltr5.getText());
		ltr5.setOnAction(action5);
		BtnAction action6 = new BtnAction(input, ltr6.getText());
		ltr6.setOnAction(action6);
		BtnAction action7 = new BtnAction(input, ltr7.getText());
		ltr7.setOnAction(action7);
		
		SubmitAction action8 = new SubmitAction(input, serverMsg, this.userScore, cl);
		submit.setOnAction(action8);
		
		//extra properties
		msg.setEditable(false);
		serverMsg.setEditable(false);
		this.userScore.setEditable(false);
		
		//build interface
		centerLetter.getChildren().add(msg);
		buttons.getChildren().addAll(ltr1, ltr2, ltr3, ltr4, ltr5, ltr6, ltr7);
		inputWord.getChildren().add(input);
		submitBtn.getChildren().add(submit);
		statusMsgs.getChildren().addAll(serverMsg, this.userScore);
		overall.getChildren().addAll(centerLetter, buttons, inputWord, submitBtn, statusMsgs);
		root.getChildren().add(overall);
		
		//set the content
		this.setContent(root); 
	}
	
	/**
	 * This method returns the TextField of the user's score. To be used for the backend.
	 * @return TextField of the user score
	 */
	public TextField getScoreField()
	{
		return this.userScore;
	}
}
