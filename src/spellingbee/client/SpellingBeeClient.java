package spellingbee.client;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.*;
import spellingbee.network.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * The SpellingBeeClient is a JavaFX GUI designed for user interaction. In it, the user
 * can play a Spelling Bee type game.
 * @author Vladyslav Berezhnyak
 */
public class SpellingBeeClient extends Application
{
	//implement THIS in the TAB youre working on
	private Client cl = new Client();
	
	@Override
	public void start(Stage stage)
	{
		//container and layout
		Group root = new Group();
		
		//tab pane and tabs
		TabPane tp = new TabPane();
		GameTab gameTab = new GameTab(cl);
		ScoreTab scoreTab = new ScoreTab(cl);
		tp.getTabs().add(gameTab);
		tp.getTabs().add(scoreTab);
		tp.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		
		//build interface
		root.getChildren().add(tp);
		
		//event listener for score
		gameTab.getScoreField().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				scoreTab.refresh();
			}
		});
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 800, 600);
		scene.setFill(Color.WHITE);
		
		//associate scene to stage and show
		stage.setTitle("Spelling Bee!");
		stage.setScene(scene);
		stage.show();
	}
	
	public static void main(String[] args)
	{
		Application.launch(args);
	}
}
