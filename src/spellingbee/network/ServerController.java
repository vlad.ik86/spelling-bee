package spellingbee.network;
import spellingbee.server.*;
import spellingbee.client.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 * @author Vladyslav Berezhnyak
 */
public class ServerController 
{
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @author Vladyslav Berezhnyak
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) 
	{
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )
		 */
		
		if(inputLine.matches("wordWorth(.*)"))
		{
			String[] query = inputLine.split(";");
			return "worth;" + Integer.toString(spellingBee.getPointsForWords(query[1]));
		}
		
		else if(inputLine.matches("wordValid(.*)"))
		{
			String[] query = inputLine.split(";");
			String validity = spellingBee.getMessage(query[1]);
			
			if(validity.equals("valid"))
			{
				return validity + ";" + Integer.toString(spellingBee.getPointsForWords(query[1]));
			}
			
			else
			{
				return validity + ";" + Integer.toString(spellingBee.getPointsForWords(""));
			}
		}
		
		else if(inputLine.equals("getLetters"))
		{
			return "letters;" + spellingBee.getAllLetters();
		}
		
		else if(inputLine.equals("getCenter"))
		{
			return "center;" + Character.toString(spellingBee.getCenterLetter());
		}
		
		else if(inputLine.equals("getUserScore"))
		{
			return "score;" + Integer.toString(spellingBee.getScore());
		}
		
		else if(inputLine.equals("getBracket"))
		{
			int[] brackets = spellingBee.getBrackets();
			String str = "";
			for(int i = 0; i < brackets.length; i++) {
				str += brackets[i] + ",";
			}
			
			return "brackets;" + str;
		}

		return "An error occured. Input did not match any requests.";
	}
}
